package bayunvnt.base_app.constant

/**
 * Created by bayunvnt on 12/21/16.
 */

object Constant {

    const val APP_NAME = "BASE_APP"
    const val NETWORK_ERROR = "Please check your network and try again"

    const val BASE_URL = "https://xxxxxxx"
    //const val BASE_URL = "http://10.90.2.171:8181/eproc_be_dev/ws/API/json/"

    const val APP_CODE = "X:XXXXX:XXXX:XXXXX"

    const val STATUS_SUCCESS = 1
    const val STATUS_ERROR = 0
    const val STATUS_TOKEN_EXPIRED = 2

    // global topic to receive app wide push notifications
    val TOPIC_GLOBAL = "global"

    // broadcast receiver intent filters
    val REGISTRATION_COMPLETE = "registrationComplete"
    val PUSH_NOTIFICATION = "pushNotification"

    // id to handle the notification in the notification tray
    val NOTIFICATION_ID = 100
    val NOTIFICATION_ID_BIG_IMAGE = 101

    val SHARED_PREF = "ah_firebase"
}
