package bayunvnt.base_app.constant

/**
 * Created by bayunvnt on 12/21/16.
 */

object ApiUri {

    const val GET_ACCESSTOKEN = "getAccessToken"
    const val GET_AUTH = "getAuth"
}
