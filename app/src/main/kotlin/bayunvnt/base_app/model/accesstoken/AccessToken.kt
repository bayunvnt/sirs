package bayunvnt.base_app.model.accesstoken

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by bayunvnt on 12/21/16.
 */

class AccessToken {

    @SerializedName("access_token")
    @Expose
    var accessToken: String? = null
    @SerializedName("info")
    @Expose
    var info: String? = null
}
