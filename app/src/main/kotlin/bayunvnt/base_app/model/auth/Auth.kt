package bayunvnt.base_app.model.auth

import com.google.gson.annotations.SerializedName

open class Auth(

	@SerializedName("EMPLOYEE_DIVISION_CODE")
	var eMPLOYEEDIVISIONCODE: String? = null,

	@SerializedName("USER_CREATE_BY")
	var uSERCREATEBY: Any? = null,

	@SerializedName("USER_STATUS")
	var uSERSTATUS: String? = null,

	@SerializedName("EXTENSION_NUMBER")
	var eXTENSIONNUMBER: String? = null,

	@SerializedName("USER_ID")
	var uSERID: String? = null,

	@SerializedName("USER_INV_TYPE_ID")
	var uSERINVTYPEID: Any? = null,

	@SerializedName("ACCESS_NAME")
	var aCCESSNAME: String? = null,

	@SerializedName("USER_LOG_CODE")
	var uSERLOGCODE: Any? = null,

	@SerializedName("POSITION_SHORTNAME")
	var pOSITIONSHORTNAME: String? = null,

	@SerializedName("USER_EMPLOYEE_CODE")
	var uSEREMPLOYEECODE: String? = null,

	@SerializedName("USER_PASSWORD")
	var uSERPASSWORD: Any? = null,

	@SerializedName("USER_LAST_LOGIN")
	var uSERLASTLOGIN: String? = null,

	@SerializedName("USER_LEVEL")
	var uSERLEVEL: String? = null,

	@SerializedName("DIVISION_SHORTNAME")
	var dIVISIONSHORTNAME: String? = null,

	@SerializedName("USER_LEVEL_NAME")
	var uSERLEVELNAME: String? = null,

	@SerializedName("USER_LAST_ONLINE")
	var uSERLASTONLINE: Any? = null,

	@SerializedName("EMPLOYEE_DEPARTMENT_CODE")
	var eMPLOYEEDEPARTMENTCODE: String? = null,

	@SerializedName("EMPLOYEE_POSITION_CODE")
	var eMPLOYEEPOSITIONCODE: String? = null,

	@SerializedName("USER_ACCESS_ID")
	var uSERACCESSID: String? = null,

	@SerializedName("USER_NAME")
	var uSERNAME: String? = null,

	@SerializedName("new_token")
	var newToken: String? = null,

	@SerializedName("USER_UPDATE_DATE")
	var uSERUPDATEDATE: String? = null,

	@SerializedName("INVTYPE_NAME")
	var iNVTYPENAME: Any? = null,

	@SerializedName("EMPLOYEE_NIK")
	var eMPLOYEENIK: String? = null,

	@SerializedName("EMPLOYEE_EMAIL")
	var eMPLOYEEEMAIL: String? = null,

	@SerializedName("USER_TYPE_NAME")
	var uSERTYPENAME: String? = null,

	@SerializedName("DEPARTMENT_SHORTNAME")
	var dEPARTMENTSHORTNAME: String? = null,

	@SerializedName("USER_UPDATE_BY")
	var uSERUPDATEBY: String? = null,

	@SerializedName("USER_TYPE")
	var uSERTYPE: String? = null,

	@SerializedName("USER_STATUS_NAME")
	var uSERSTATUSNAME: String? = null,

	@SerializedName("USER_ONLINE")
	var uSERONLINE: String? = null,

	@SerializedName("USER_CREATE_DATE")
	var uSERCREATEDATE: Any? = null
)