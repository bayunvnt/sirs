package bayunvnt.base_app.model

import javax.annotation.Generated
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource

@Generated("com.robohorse.robopojogenerator")
open class MapProjectPolylines(
		var id: String = "",
		var fillLayerPointList: MutableList<Point> = arrayListOf(),
		var lineLayerPointList: MutableList<Point> = arrayListOf(),
		var circleLayerFeatureList: MutableList<Feature> = arrayListOf(),
		var listOfList: MutableList<MutableList<Point>> = arrayListOf(),
		var circleSource: GeoJsonSource? = null,
		var fillSource: GeoJsonSource? = null,
		var lineSource: GeoJsonSource? = null,
		var CIRCLE_SOURCE_ID: String = "",
		var FILL_SOURCE_ID: String  = "",
		var LINE_SOURCE_ID: String  = "",
		var CIRCLE_LAYER_ID: String  = "",
		var FILL_LAYER_ID: String  = "",
		var LINE_LAYER_ID: String  = "",
		var coordinat: List<LatLng>? = arrayListOf(),
		var firstPointOfPolygon: Point? = null,
		var type: String? = null
)