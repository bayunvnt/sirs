package bayunvnt.base_app.model

import com.mapbox.mapboxsdk.geometry.LatLng

data class DataLocation(
        val id: String,
        val name: String?,
        val luas: String,
        val coordinat: List<LatLng>? = arrayListOf()
)