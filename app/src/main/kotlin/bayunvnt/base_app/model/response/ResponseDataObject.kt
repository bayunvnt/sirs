package bayunvnt.base_app.model.response


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import bayunvnt.base_app.base.BaseResponse

/**
 * Created by bayunvnt on 12/21/16.
 */

class ResponseDataObject<T> : BaseResponse() {

    @SerializedName("results")
    @Expose
    var result: T? = null
}
