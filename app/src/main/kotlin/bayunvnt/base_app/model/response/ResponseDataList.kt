package bayunvnt.base_app.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import bayunvnt.base_app.base.BaseResponse


/**
 * Created by bayunvnt on 12/23/16.
 */

class ResponseDataList<T> : BaseResponse() {
    @SerializedName("results")
    @Expose
    var result: List<T>? = null
}
