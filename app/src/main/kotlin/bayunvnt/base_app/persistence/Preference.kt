package bayunvnt.base_app.persistence

import bayunvnt.base_app.model.DataLocation
import com.orhanobut.hawk.Hawk

import bayunvnt.base_app.model.accesstoken.AccessToken
import bayunvnt.base_app.model.auth.Auth


/**
 * Created by bayunvnt on 12/21/16.
 */

class Preference {

    object Key {
        val ACCESS_TOKEN = "Key.AccessToken"
        val AUTH = "Key.Auth"
        val TOKEN = "token"
        val LOCATION = "Key.Location"
    }

    companion object {

        fun saveAccessToken(accessToken: AccessToken) {
            Hawk.put(Key.ACCESS_TOKEN, accessToken)
        }

        val accessToken: AccessToken
            get() = Hawk.get(Key.ACCESS_TOKEN)

        fun saveDataLocation(loc: MutableList<DataLocation>) {
            Hawk.put(Key.LOCATION, loc)
        }

        val getDataLocation: MutableList<DataLocation>?
            get() = Hawk.get(Key.LOCATION)

        fun saveAuth(auth: Auth) {
            Hawk.put(Key.AUTH, auth)
        }

        val auth: Auth
            get() = Hawk.get(Key.AUTH)
    }
}
