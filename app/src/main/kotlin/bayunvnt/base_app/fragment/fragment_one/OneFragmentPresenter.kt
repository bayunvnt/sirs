package bayunvnt.base_app.fragment.fragment_one

import bayunvnt.base_app.base.BasePresenter

/**
 * Created by bayunvnt on 09/10/17.
 */

class OneFragmentPresenter : BasePresenter() {


   /* fun getSongByGenre(accessToken: String, genre: String) {
        if (!Utils.isNetworkConnected(mContext)) {
            song().onFailed(mContext.getString(R.string.noConnection))
            return
        }
        song().onShowLoading()
        dataManager.getSongByGenre(accessToken, genre)
                .doOnTerminate({ song().onDismissLoading() })
                .compose(RxUtils.applyApiCall())
                .subscribe({ opportunityResponseResponseDataObject ->
                    if (opportunityResponseResponseDataObject.status === Constant.STATUS_TOKEN_EXPIRED)
                        song().onTokenExpired()
                    else {
                        if (opportunityResponseResponseDataObject.status === Constant.STATUS_SUCCESS) {
                            song().onSuccessListSong(opportunityResponseResponseDataObject.result!!)
                        } else {
                            song().onFailed(opportunityResponseResponseDataObject.message!!)
                        }
                    }
                }, { throwable -> ErrorHandler.handlerErrorPresenter(song(), throwable) })
    }*/

    internal fun fragmentOneView(): OneFragmentView {
        return getView()
    }
}
