package bayunvnt.base_app.fragment.fragment_one

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bayunvnt.base_app.R
import bayunvnt.base_app.base.BaseFragment


/**
 * Created by bayunvnt on 09/10/17.
 */

class OneFragment : BaseFragment(), OneFragmentView {

    lateinit var viewx:View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        viewx = inflater!!.inflate(R.layout.fragment_one, container, false)

        return viewx
    }

    override fun onShowLoading() {

     }

    override fun onDismissLoading() {

    }

    override fun onTokenExpired() {

    }

    override fun onFailed(message: String) {

    }
}
