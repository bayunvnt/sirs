package bayunvnt.base_app.utils


import android.os.Looper
import android.util.Log
import bayunvnt.base_app.BaseApp
import bayunvnt.base_app.base.BaseResponse
import bayunvnt.base_app.utils.network.BaseAppException
import retrofit2.adapter.rxjava.HttpException
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.IOException

/**
 * Created by bayunvnt on 09/10/17.
 */

object RxUtils {

    /*public static Observable.Transformer applySchedulers = new Observable.Transformer() {
        @Override
        public Object call(Object o){
            return null;
        }
    };*/
    fun <T> applyScheduler(): Observable.Transformer<T, T> {
        return Observable.Transformer { tObservable ->
            tObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
        }
    }

    fun <T> applyApiCall(): Observable.Transformer<T, T> {
        return Observable.Transformer { tObservable ->
            tObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .onErrorResumeNext { throwable ->

                        Logger.log(throwable)
                        if (throwable is HttpException) {
                            try {
                                val res = throwable.response().errorBody().string()

                                val gson = BaseApp.component!!.gson()
                                val baseResponse = gson.fromJson(res, BaseResponse::class.java)
                                val e = BaseAppException(baseResponse, throwable.response().code())
                                return@onErrorResumeNext tObservable.subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                            } catch (e: IOException) {
                                return@onErrorResumeNext tObservable.subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                            }

                        } else {
                            return@onErrorResumeNext tObservable.subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())

                        }
                    }
        }
    }

    fun checkMainThread() {
        val isMainThread = Looper.myLooper() == Looper.getMainLooper()
        Logger.log(Log.DEBUG, "RX___ Is main thread :" + isMainThread)
    }
}
