package bayunvnt.base_app.utils.dialog

import android.content.Context

import com.afollestad.materialdialogs.MaterialDialog

/**
 * Created by bayunvnt on 12/22/16.
 */

object MessageDialog {
    fun showMessage(context: Context, message: Int) {
        showMessage(context, context.getString(message))
    }

    fun showMessage(context: Context, message: String) {
        MaterialDialog.Builder(context)
                .content(message)
                .positiveText("Ok")
                .onPositive { dialog, _ -> dialog.dismiss() }
                .show()
    }
}
