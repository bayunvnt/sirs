package bayunvnt.base_app.utils.network


import bayunvnt.base_app.base.BaseResponse

/**
 * Created by bayunvnt on 12/21/16.
 */

class BaseAppException(val response: BaseResponse, val responseCode: Int) : Exception()
