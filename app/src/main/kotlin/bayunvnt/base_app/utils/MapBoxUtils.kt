package bayunvnt.base_app.utils

import android.content.Context
import android.util.Log
import android.widget.Toast
import bayunvnt.base_app.R
import cn.pedant.SweetAlert.SweetAlertDialog
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.geometry.LatLngQuad
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.offline.*
import com.mapbox.mapboxsdk.style.layers.RasterLayer
import com.mapbox.mapboxsdk.style.sources.ImageSource
import org.json.JSONObject
import java.net.URL
import kotlin.math.roundToInt


class MapBoxUtils {
    fun addImageOverlay(style: Style,
                        latlng1: LatLng,
                        latlng2: LatLng,
                        latlng3: LatLng,
                        latlng4: LatLng,
                        ID_IMAGE_SOURCE: String,
                        ID_IMAGE_LAYER: String,
                        sourceImage: Any) {


        val quad = LatLngQuad(
                latlng1,
                latlng2,
                latlng3,
                latlng4
        )

        when (sourceImage) {
            is String -> {
                style.addSource(ImageSource(ID_IMAGE_SOURCE, quad, URL(sourceImage.toString())))
                style.addLayer(RasterLayer(ID_IMAGE_LAYER, ID_IMAGE_SOURCE))
            }
            is Int -> {
                style.addSource(ImageSource(ID_IMAGE_SOURCE, quad, sourceImage.toString().toInt()))
                style.addLayer(RasterLayer(ID_IMAGE_LAYER, ID_IMAGE_SOURCE))
            }
            else -> {
                Log.d("mapbox layer","failed add layout image")
            }
        }
    }

    private var offlineManager: OfflineManager? = null
    private var offlineRegion: OfflineRegion? = null
    val JSON_CHARSET = "UTF-8"
    val JSON_FIELD_REGION_NAME = "FIELD_REGION_NAME"

    fun saveOfflineMap(context: Context,
                       regionName: String){

        Mapbox.getInstance(context, context.getString(R.string.mapbox_access_token))

        // Set up the OfflineManager
        offlineManager = OfflineManager.getInstance(context)

        // Create offline definition using the current
        // style and boundaries of visible map area
        val styleUrl = Style.LIGHT
        val bounds: LatLngBounds = LatLngBounds.Builder()
                .include(LatLng(-6.952364, 107.682852))
                .include(LatLng(-6.952364, 107.682852)).build()
        val minZoom = 10.0
        val maxZoom = 18.0
        val pixelRatio: Float = context.resources.displayMetrics.density
        val definition = OfflineTilePyramidRegionDefinition(
                styleUrl, bounds, minZoom, maxZoom, pixelRatio)

        // Build a JSONObject using the user-defined offline region title,
        // convert it into string, and use it to create a metadata variable.
        // The metadata variable will later be passed to createOfflineRegion()
        val metadata: ByteArray?
        metadata = try {
            val jsonObject = JSONObject()
            jsonObject.put(JSON_FIELD_REGION_NAME, regionName)
            val json = jsonObject.toString()
            json.toByteArray(charset(JSON_CHARSET))
        } catch (exception: java.lang.Exception) {
            Log.d("Failed to download map", exception.message)
            null
        }

        // Create the offline region and launch the download
        offlineManager?.createOfflineRegion(definition, metadata!!, object : OfflineManager.CreateOfflineRegionCallback {
            override fun onCreate(offlineRegion: OfflineRegion) {
                Log.d("Offline region created:", regionName)
                this@MapBoxUtils.offlineRegion = offlineRegion
                launchDownload()
            }

            override fun onError(error: String) {
                Log.d("Error: %s", error)
            }
        })
    }

    private fun launchDownload() {
        // Set up an observer to handle download progress and
        // notify the user when the region is finished downloading
        offlineRegion!!.setObserver(object : OfflineRegion.OfflineRegionObserver {
            override fun onStatusChanged(status: OfflineRegionStatus) {
                // Compute a percentage
                val percentage = if (status.requiredResourceCount >= 0) 100.0 * status.completedResourceCount / status.requiredResourceCount else 0.0
                if (status.isComplete) {
                    // Download complete
                    sweetAlretLoading?.dismiss()
                    return
                } else if (status.isRequiredResourceCountPrecise) {
                    // Switch to determinate state
//                    setPercentage(percentage.roundToInt())
                    Log.d("counterxx", percentage.roundToInt().toString())
                }

                // Log what is being currently downloaded
//                Log.d("bytes downloaded.", status.completedResourceCount.toString(), status.requiredResourceCount.toString(), status.completedResourceSize.toString())
            }

            override fun onError(error: OfflineRegionError) {
                Log.d("onError reason: %s", error.reason)
                Log.d("onError message: %s", error.message)
            }

            override fun mapboxTileCountLimitExceeded(limit: Long) {
                Log.d("Mapbox tile count limit", limit.toString())
            }
        })

        // Change the region state
        offlineRegion!!.setDownloadState(OfflineRegion.STATE_ACTIVE)
    }

    private fun downloadedRegionList(context: Context) {
        // Query the DB asynchronously
        offlineManager = OfflineManager.getInstance(context)
        offlineManager?.listOfflineRegions(object : OfflineManager.ListOfflineRegionsCallback {
            override fun onList(offlineRegions: Array<OfflineRegion>) {
                // Check result. If no regions have been
                // downloaded yet, notify user and return
                if (offlineRegions.isEmpty()) {
                    Toast.makeText(context, "Kosong", Toast.LENGTH_SHORT).show()
                    return
                }

                // Add all of the region names to a list
                val offlineRegionsNames = java.util.ArrayList<String>()
                for (offlineRegion in offlineRegions) {
                    offlineRegionsNames.add(getRegionName(offlineRegion))
                }
                val items = offlineRegionsNames.toTypedArray<CharSequence>()
                Toast.makeText(context, items.size.toString(), Toast.LENGTH_SHORT).show()
            }

            override fun onError(error: String) {
                Log.d("Error: %s", error)
            }
        })
    }

    private fun getRegionName(offlineRegion: OfflineRegion): String {
        // Get the region name from the offline region metadata
        val regionName: String
        regionName = try {
            val metadata = offlineRegion.metadata
            val json = String(metadata, charset(JSON_CHARSET))
            val jsonObject = JSONObject(json)
            jsonObject.getString(JSON_FIELD_REGION_NAME)
        } catch (exception: java.lang.Exception) {
            String.format("Region ", offlineRegion.id)
        }
        return regionName
    }

    fun getOfflineMap(mapboxMap: MapboxMap,
                      context: Context,
                      onRegionNotEmpty: (Style) -> Unit){
        val offlineManager = OfflineManager.getInstance(context)
        offlineManager?.listOfflineRegions(object : OfflineManager.ListOfflineRegionsCallback {
            override fun onList(offlineRegions: Array<OfflineRegion>) {
                if (offlineRegions.isEmpty()) {
                    return
                }

                // Add all of the region names to a list
                val offlineRegionsNames = java.util.ArrayList<String>()
                for (offlineRegion in offlineRegions) {
                    offlineRegionsNames.add("getRegionName(offlineRegion)")
                }
                mapboxMap.setOfflineRegionDefinition(offlineRegions[0].definition)

                mapboxMap.setStyle(offlineRegions[0].definition.styleURL){style ->
                    Log.d("lastknownloc1","asd")
                    onRegionNotEmpty(style)
                }
            }

            override fun onError(error: String) {
                Log.d("Error: %s", error)
            }
        })
    }

    var sweetAlretLoading: SweetAlertDialog? = null
    private fun showLoading(context: Context) {
        sweetAlretLoading = SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE)
        sweetAlretLoading?.titleText = "SYNCHRONOUS DATA"
        sweetAlretLoading?.setCancelable(false)
        sweetAlretLoading?.confirmText = "OK"

        sweetAlretLoading?.show()
    }
    private fun hideLoading(){
        if(sweetAlretLoading != null){
            sweetAlretLoading?.dismiss()
        }
    }
}