package bayunvnt.base_app.utils


import bayunvnt.base_app.constant.Constant
import bayunvnt.base_app.listener.MvpView
import bayunvnt.base_app.utils.network.BaseAppException
import retrofit2.adapter.rxjava.HttpException

/**
 * Created by bayunvnt on 12/21/16.
 */

object ErrorHandler {
    fun handlerErrorPresenter(mvpView: MvpView, throwable: Throwable) {
        val message = ErrorHandler.handleError(throwable)
        mvpView.onFailed(message!!)

    }

    fun handleError(throwable: Throwable?): String? {
        Logger.log(throwable)

        if (throwable == null) return Constant.NETWORK_ERROR
        if (throwable is BaseAppException) {
            val cetunException = throwable as BaseAppException?
            val baseResponse = cetunException!!.response
            return baseResponse.message
        }

        if (throwable is HttpException) {
            val httpException = throwable as HttpException?
            if (httpException!!.code() == 403) {
                forceLogout()
                return null
            }
        }
        return Constant.NETWORK_ERROR
    }

    private fun forceLogout() {

    }
}
