package bayunvnt.base_app.utils

import android.content.Context
import android.net.ConnectivityManager
import android.preference.PreferenceManager
import android.util.Log
import android.util.TypedValue
import bayunvnt.base_app.persistence.Preference


/**
 * Created by bayunvnt on 12/22/16.
 */

object Utils {


    fun getislogin(context: Context): String {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString("islogin", "")
    }

    fun saveislogin(log: String, context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString("islogin", log)
        editor.commit()
    }

    /**
     * Check if network is connected
     */
    fun isNetworkConnected(ctx: Context): Boolean {
        val cm = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (cm != null) {
            val ni = cm.activeNetworkInfo

            return ni != null
        } else {
            return false
        }
    }


    fun getToken(context: Context): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(Preference.Key.TOKEN, null)
    }

    fun saveToken(activity: Context, value: String) {
        Log.d("SPMANAGER", "saving" + value + " for key " + Preference.Key.TOKEN)
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity)
        val editor = sharedPreferences.edit()
        editor.putString(Preference.Key.TOKEN, value)
        if (editor.commit()) {
            Log.d("SPMANAGER", "commited" + value)
        } else {
            Log.d("SPMANAGER", "not commited")
        }
    }
}
