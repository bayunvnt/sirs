package bayunvnt.base_app.network

import bayunvnt.base_app.constant.Constant
import bayunvnt.base_app.model.accesstoken.AccessToken
import bayunvnt.base_app.model.auth.Auth
import bayunvnt.base_app.model.response.ResponseDataObject
import rx.Observable
import javax.inject.Inject


/**
 * Created by bayunvnt on 12/21/16.
 */

class DataManager @Inject
constructor(private val apiService: ApiService) {

    val accessToken: Observable<ResponseDataObject<AccessToken>>
        get() = apiService.getAccessToken(Constant.APP_CODE)

    fun getAuth(username: String, password: String, access_token: String, remember: String): Observable<ResponseDataObject<Auth>> {
        return apiService.getAuth(username, password, access_token, remember)
    }


    /*fun getAuction(token: String, user_id: String, offset: String): Observable<ResponseDataObject<AuctionData>> {
        return apiService.getAuction(token, user_id, offset)
    }

    fun getStepAuction(token: String, user_id: String, url: String): Observable<ResponseDataObject<StepData>> {
        return apiService.getStepAuction(token, user_id, url)
    }*/

}
