package bayunvnt.base_app.network

import bayunvnt.base_app.constant.ApiUri
import bayunvnt.base_app.model.accesstoken.AccessToken
import bayunvnt.base_app.model.auth.Auth
import bayunvnt.base_app.model.response.ResponseDataObject
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import rx.Observable

/**
 * Created by bayunvnt on 12/21/16.
 */

interface ApiService {
    @FormUrlEncoded
    @POST(ApiUri.GET_ACCESSTOKEN)
    fun getAccessToken(@Field("app_code") app_code: String): Observable<ResponseDataObject<AccessToken>>

    @FormUrlEncoded
    @POST(ApiUri.GET_AUTH)
    fun getAuth(
            @Field("username") username: String,
            @Field("password") password: String,
            @Field("access_token") access_token: String,
            @Field("remember") remember: String): Observable<ResponseDataObject<Auth>>
}
