package bayunvnt.base_app

import android.app.Application
import android.content.Context
import com.orhanobut.hawk.Hawk
import bayunvnt.base_app.dagger.component.AppComponent

/**
 * Created by Anonymous on 9/11/2017.
 */

open class BaseApp : Application() {
    override fun onCreate() {
        super.onCreate()
        setupHawk()

        component = AppComponent.Initializer.init(this)
        component!!.inject(this)
        context = applicationContext

    }


    open fun setupHawk() {
        Hawk.init(this)
                .build()
    }

    companion object {
        var component: AppComponent? = null
            private set
        var context: Context? = null
            private set
    }

}
