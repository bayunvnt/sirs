package bayunvnt.base_app.permission

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.SparseIntArray
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import bayunvnt.base_app.base.BaseActivity


/**
 * Created by bayunvnt on 12/21/16.
 */

abstract class AbsRuntimePermission : BaseActivity() {

    private var mErrorString: SparseIntArray? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mErrorString = SparseIntArray()
    }

    abstract fun onPermissionsGranted(requestCode: Int)

    fun requestAppPermissions(requestedPermission: Array<String>, stringId: Int, requestCode: Int) {
        mErrorString!!.put(requestCode, stringId)

        var permissionCheck = PackageManager.PERMISSION_GRANTED
        var showRequestPermissions = false
        for (permission in requestedPermission) {
            permissionCheck = permissionCheck + ContextCompat.checkSelfPermission(this, permission)
            showRequestPermissions = showRequestPermissions || ActivityCompat.shouldShowRequestPermissionRationale(this, permission)
        }

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (showRequestPermissions) {
//                Snackbar.make(findViewById(android.R.id.content), stringId, Snackbar.LENGTH_INDEFINITE).setAction("GRANT") { ActivityCompat.requestPermissions(this@AbsRuntimePermission, requestedPermission, requestCode) }.show()
            } else {
                ActivityCompat.requestPermissions(this, requestedPermission, requestCode)
            }
        } else {
            onPermissionsGranted(requestCode)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var permissionCheck = PackageManager.PERMISSION_GRANTED

        for (permission in grantResults) {
            permissionCheck = permissionCheck + permission
        }

        if (grantResults.size > 0 && PackageManager.PERMISSION_GRANTED == permissionCheck) {
            onPermissionsGranted(requestCode)
        } else {
//            Snackbar.make(findViewById(android.R.id.content), mErrorString!!.get(requestCode),
//                    Snackbar.LENGTH_INDEFINITE).setAction("ENABLE") {
//                val i = Intent()
//                i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
//                i.data = Uri.parse("package:" + packageName)
//                i.addCategory(Intent.CATEGORY_DEFAULT)
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
//                i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
//                startActivity(i)
//            }.show()
        }
    }
}
