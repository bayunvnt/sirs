package bayunvnt.base_app.listener

/**
 * Created by anonymous on 12/21/16.
 */

interface MvpView {

    fun onShowLoading()

    fun onDismissLoading()

    fun onTokenExpired()

    fun onFailed(message: String)
}
