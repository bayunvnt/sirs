package bayunvnt.base_app.listener

/**
 * Created by bayunvnt on 12/21/16.
 */

interface OnLoadMoreListener {

    fun onLoadMore()
}
