package bayunvnt.base_app.base

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.Toast

/**
 * Created by bayunvnt on 12/21/16.
 */

class BaseDialogFragment : DialogFragment() {

    protected var mContext: Context? = null
    protected var mLayoutInflater: LayoutInflater? = null

    protected val defaultDialog: Dialog
        get() {
            val dialog = Dialog(mContext!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
            return dialog
        }

    val baseActivity: BaseActivity
        get() = activity as BaseActivity


    fun showToast(message: String) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show()
    }
}
