package bayunvnt.base_app.base

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater

import com.trello.rxlifecycle.components.support.RxFragment

/**
 * Created by bayunvnt on 12/21/16.
 */

open class BaseFragment : RxFragment() {

    protected var mContect: Activity? = null
    protected var mInflater: LayoutInflater? = null

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mContect = activity
        mInflater = LayoutInflater.from(activity)
    }
}
