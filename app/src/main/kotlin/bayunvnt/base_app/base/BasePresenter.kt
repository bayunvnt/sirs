package bayunvnt.base_app.base

import android.content.Context
import bayunvnt.base_app.BaseApp
import bayunvnt.base_app.listener.MvpView
import bayunvnt.base_app.network.ApiService
import bayunvnt.base_app.network.DataManager
import javax.inject.Inject


/**
 * Created by bayu on 12/21/16.
 */


open class BasePresenter {

    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var mApiService: ApiService
    @Inject
    lateinit var dataManager: DataManager
    private var mvpView: MvpView? = null

    init {
        BaseApp.component?.inject(this)
    }

    // TODO must be change to generic type
    fun attachView(mvpView: MvpView) {
        this.mvpView = mvpView
    }

    fun dettachView() {
        this.mvpView = null
    }


    fun <T : MvpView> getView(): T {
        return mvpView as T
    }
}
