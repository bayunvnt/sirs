package bayunvnt.base_app.base

import com.google.gson.annotations.SerializedName

/**
 * Created by bayunvnt on 12/21/16.
 */

open class BaseResponse {
    @SerializedName("status")
    var status: Int = 0
    @SerializedName("code")
    var code: Int = 0
    @SerializedName("message")
    var message: String? = null
    @SerializedName("generated")
    var generated: Double = 0.toDouble()
    @SerializedName("serverTime")
    var serverTime: Int = 0

    @SerializedName("total_data")
    var total_data: Int = 0
}
