package bayunvnt.base_app.base

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import java.util.*

/**
 * Created by bayunvnt on 12/21/16.
 */

abstract class BaseAdapter<T, VH : BaseHolder>(protected var mContext: Context, private val mResId: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mListData = ArrayList<T>()
    protected var mInflater: LayoutInflater

    protected var mListener: OnItemClickListener? = null

    val listData: List<T>
        get() = mListData

    init {
        this.mInflater = LayoutInflater.from(mContext)
    }

    fun setListener(baseFragment: BaseFragment) {
        mListener = baseFragment as OnItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (mResId == 0)
            onCreate(null, viewType)
        else
            onCreate(mInflater.inflate(mResId, parent, false), viewType)
    }

    abstract fun onCreate(view: View?, viewType: Int): RecyclerView.ViewHolder

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        onBind(holder, mListData[position], position)
    }

    protected abstract fun onBind(vh: RecyclerView.ViewHolder, t: T, position: Int)

    override fun getItemCount(): Int {
        return mListData.size
    }

    fun getItem(position: Int): T {
        return mListData[position]
    }

    fun pushData(listData: List<T>) {
        mListData.clear()
        mListData.addAll(listData)
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onItemClickListener(position: Int)
    }
}
