package bayunvnt.base_app.base

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import android.widget.Toast

import com.trello.rxlifecycle.components.support.RxAppCompatActivity

import bayunvnt.base_app.R

/**
 * Created by bayunvnt on 12/21/16.
 */

open class BaseActivity : RxAppCompatActivity() {

    private var progressDialog: ProgressDialog? = null

    protected var mTfLight: Typeface? = null

    val baseFragmentManager: FragmentManager
        get() = supportFragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mTfLight = Typeface.createFromAsset(assets, "OpenSans-Light.ttf")
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(newBase)
    }

    open fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    open fun showLoading() {
        progressDialog = ProgressDialog.show(this, null, resources.getString(R.string.loading), true, true)
    }

    open fun dismissLoading() {
        progressDialog!!.dismiss()
    }

}
