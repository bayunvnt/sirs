package bayunvnt.base_app.dagger.modules

import androidx.fragment.app.FragmentManager
import dagger.Module
import dagger.Provides
import bayunvnt.base_app.base.BaseActivity
import bayunvnt.base_app.dagger.ActivityContext

/**
 * Created by bayunvnt on 12/21/16.
 */
@Module
class ActivityModule(private val baseActivity: BaseActivity) {

    @Provides
    @ActivityContext
    internal fun provideActivity(): BaseActivity {
        return baseActivity
    }

    @Provides
    @ActivityContext
    internal fun provideFragmentManager(): FragmentManager {
        return baseActivity.baseFragmentManager
    }
}
