package bayunvnt.base_app.dagger.modules

import android.util.Log

import com.google.gson.Gson
import com.google.gson.GsonBuilder

import java.lang.reflect.Modifier
import java.util.concurrent.TimeUnit

import dagger.Module
import dagger.Provides
import bayunvnt.base_app.constant.Constant
import bayunvnt.base_app.network.ApiService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by bayunvnt on 12/21/16.
 */
@Module
class NetworkModule {

    @Provides
    internal fun provideGson(): Gson {
        return Gson()
    }

    @Provides
    internal fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
                        .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                        .serializeNulls()
                        .create()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
    }

    @Provides
    internal fun provideOkHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor { message -> Log.d("LogIntercep", message) }
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
                .writeTimeout(25, TimeUnit.SECONDS)
                .readTimeout(25, TimeUnit.SECONDS)
                .connectTimeout(25, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .addNetworkInterceptor { chain ->
                    val request = chain.request()

                    val newRequest = request.newBuilder()
                            //                            .addHeader("Accept", "application/json")
                            //                            .addHeader("Authorization","Basic NTc2YWNjNDkzNTU0MTA5MDc5ODI2NWFkOk94MXJaeWxRaFd2OGp1UmFmS0JnaUc5YldSbGw4TUo5")
                            //                            .addHeader("Cache-Control", "no-cache")
                            //                            .addHeader("Cache-Control", "no-store")
                            .build()
                    chain.proceed(newRequest)
                }.build()
    }

    @Provides
    internal fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java!!)
    }
}
