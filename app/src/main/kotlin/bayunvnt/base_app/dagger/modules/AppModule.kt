package bayunvnt.base_app.dagger.modules

import android.content.Context
import android.os.Handler
import android.os.Looper

import dagger.Module
import dagger.Provides

/**
 * Created by bayunvnt on 12/21/16.
 */
@Module
open class AppModule(private val mContext: Context) {

    @Provides
    internal fun provideContext(): Context {
        return mContext
    }

    @Provides
    internal fun provideHandler(): Handler {
        return Handler(Looper.getMainLooper())
    }
}
