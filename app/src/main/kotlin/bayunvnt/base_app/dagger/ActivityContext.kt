package bayunvnt.base_app.dagger

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

import javax.inject.Scope

/**
 * Created by bayunvnt on 12/21/16.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
annotation class ActivityContext
