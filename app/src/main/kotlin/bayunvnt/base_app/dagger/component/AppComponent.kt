package bayunvnt.base_app.dagger.component

import android.content.Context
import android.os.Handler
import com.google.gson.Gson
import dagger.Component
import bayunvnt.base_app.BaseApp
import bayunvnt.base_app.base.BasePresenter
import bayunvnt.base_app.dagger.modules.AppModule
import bayunvnt.base_app.dagger.modules.NetworkModule
import bayunvnt.base_app.network.DataManager

/**
 * Created by bayunvnt on 12/21/16.
 */
@Component(modules = arrayOf(AppModule::class, NetworkModule::class))
interface AppComponent {

    fun inject(simaproApp: BaseApp)
    fun inject(basePresenter: BasePresenter)


    fun gson(): Gson
    fun datamanager(): DataManager
    fun mainHandler(): Handler

    object Initializer {

        fun init(context: Context): AppComponent {
            return DaggerAppComponent.builder()
                    .networkModule(NetworkModule())
                    .appModule(AppModule(context))
                    .build()
        }
    }
}
