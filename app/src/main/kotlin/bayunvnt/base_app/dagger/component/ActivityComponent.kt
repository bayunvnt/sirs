package bayunvnt.base_app.dagger.component

import dagger.Component
import bayunvnt.base_app.dagger.ActivityContext
import bayunvnt.base_app.dagger.modules.ActivityModule
import bayunvnt.base_app.dagger.modules.FragmentModule
import bayunvnt.base_app.dagger.modules.PresenterModule

/**
 * Created by bayunvnt on 12/21/16.
 */
@ActivityContext
@Component(modules = arrayOf(FragmentModule::class, PresenterModule::class, ActivityModule::class), dependencies = arrayOf(AppComponent::class))
interface ActivityComponent
