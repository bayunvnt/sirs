package bayunvnt.base_app.ui.splash


import android.util.Log
import bayunvnt.base_app.base.BasePresenter
import bayunvnt.base_app.constant.Constant
import bayunvnt.base_app.model.accesstoken.AccessToken
import bayunvnt.base_app.model.response.ResponseDataObject
import bayunvnt.base_app.utils.ErrorHandler
import bayunvnt.base_app.utils.RxUtils

/**
 * Created by bayunvnt on 12/21/16.
 */

class AccessTokenPresenter : BasePresenter() {

    fun getAccessToken() {
        accessTokenView().onShowLoading()
        dataManager!!.accessToken
                .doOnTerminate { accessTokenView().onDismissLoading() }
                .compose<ResponseDataObject<AccessToken>>(RxUtils.applyApiCall<ResponseDataObject<AccessToken>>())
                .subscribe({ accessTokenResponseDataObject ->
                    Log.d("asx",accessTokenResponseDataObject.status.toString())
                    if (accessTokenResponseDataObject.status == Constant.STATUS_TOKEN_EXPIRED)
                        accessTokenView().onTokenExpired()
                    else {
                        if (accessTokenResponseDataObject.status == Constant.STATUS_SUCCESS) {
                            accessTokenView().onSuccess(accessTokenResponseDataObject.result!!.accessToken!!)
                        } else {
                            accessTokenView().onFailed(accessTokenResponseDataObject.message!!)
                        }
                    }
                }
                ) { throwable -> ErrorHandler.handlerErrorPresenter(accessTokenView(), throwable) }
    }

    internal fun accessTokenView(): AccessTokenView {
        return getView()
    }
}
