package bayunvnt.base_app.ui.splash


import bayunvnt.base_app.listener.MvpView

/**
 * Created by bayunvnt on 12/21/16.
 */

interface AccessTokenView : MvpView {

    fun onSuccess(accessToken: String)
}
