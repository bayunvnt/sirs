package bayunvnt.base_app.ui.splash

import android.content.Intent
import android.os.Build
import android.os.Bundle
import bayunvnt.base_app.R
import bayunvnt.base_app.base.BaseActivity
import bayunvnt.base_app.persistence.Preference
import bayunvnt.base_app.ui.map.MapsActivityDrawingMapBox
import bayunvnt.base_app.utils.Utils
import java.util.*

class Splash : BaseActivity(), AccessTokenView {

    private var tt: TimerTask? = null
    private var timer: Timer? = null
    private var isCancelled = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    override fun onStart() {
        super.onStart()
        setupTimer()
//        getAccessToken()
    }

    private fun setupTimer() {
        timer = Timer()
        tt = object : TimerTask() {
            override fun run() {
                runApp()
            }
        }
        timer!!.schedule(tt, SPLASH_TIME.toLong())
    }

    private fun getAccessToken() {
        val accessTokenPresenter = AccessTokenPresenter()
        accessTokenPresenter.attachView(this)
        accessTokenPresenter.getAccessToken()
    }

    private fun runApp() {
        if (!isCancelled) {
            if (Utils.getislogin(applicationContext) == "1") {
//                val intent = Intent(this@Splash, MainActivity::class.java)
                startActivity(intent)
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                }
                this@Splash.finish()
            } else {
                val intent = Intent(this@Splash, MapsActivityDrawingMapBox::class.java)
                startActivity(intent)
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                }
                this@Splash.finish()
            }
        }
    }

    override fun onBackPressed() {
        isCancelled = true
        timer!!.cancel()
        finish()
    }

    override fun onShowLoading() {

    }

    override fun onDismissLoading() {

    }

    override fun onTokenExpired() {

    }

    override fun onFailed(message: String) {
        showToast(message)
    }

    override fun onSuccess(accessToken: String) {
        Utils.saveToken(this, accessToken)
        try{
            if(Preference.auth!=null)
            {
                Preference.auth.newToken = accessToken
            }
        }
        catch(e:Exception)
        {
            e.printStackTrace()
        }
        setupTimer()
    }

    companion object {
        private val SPLASH_TIME = 5000
    }
}