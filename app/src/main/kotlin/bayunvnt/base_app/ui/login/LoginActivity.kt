package bayunvnt.base_app.ui.login

import android.content.Intent
import android.os.Build
import android.os.Bundle
import bayunvnt.base_app.R
import bayunvnt.base_app.base.BaseActivity
import bayunvnt.base_app.model.auth.Auth
import bayunvnt.base_app.persistence.Preference
import bayunvnt.base_app.utils.Utils
import bayunvnt.base_app.utils.dialog.MessageDialog
import kotlinx.android.synthetic.main.activity_login.*
import java.net.URISyntaxException


class LoginActivity : BaseActivity(), AuthView {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


//        val video = Video.createVideo("https://learning-services-media.brightcove.com/videos/hls/greatblueheron/greatblueheron.m3u8",
//                DeliveryType.HLS)
//        try {
//            val myposterImage = java.net.URI("https://solutions.brightcove.com/bcls/assets/images/Great-Blue-Heron.png")
//            video.properties[Video.Fields.STILL_IMAGE_URI] = myposterImage
//        } catch (e: URISyntaxException) {
//            e.printStackTrace()
//        }
//
//        brightcoveVideoView.add(video)
//        brightcoveVideoView.start()

        //getAccessToken()
    }

    override fun onSuccess(auth: Auth) {
        Preference.saveAuth(auth)

        Utils.saveToken(this, auth.newToken!!)
        Utils.saveislogin("1", applicationContext)

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left)
        }
    }

    override fun onShowLoading() {
    }

    override fun onDismissLoading() {
    }

    override fun onTokenExpired() {

    }

    override fun onFailed(message: String) {
        MessageDialog.showMessage(this, message)
    }

    override fun onBackPressed() {
        finish()
    }

}
