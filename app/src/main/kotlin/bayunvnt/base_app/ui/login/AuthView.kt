package bayunvnt.base_app.ui.login

import bayunvnt.base_app.listener.MvpView
import bayunvnt.base_app.model.auth.Auth

/**
 * Created by bayunvnt on 12/21/16.
 */

interface AuthView : MvpView {
    fun onSuccess(auth: Auth)
}
