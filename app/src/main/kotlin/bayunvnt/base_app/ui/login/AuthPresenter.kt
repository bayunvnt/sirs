package bayunvnt.base_app.ui.login


import bayunvnt.base_app.base.BasePresenter
import bayunvnt.base_app.constant.Constant
import bayunvnt.base_app.model.auth.Auth
import bayunvnt.base_app.model.response.ResponseDataObject
import bayunvnt.base_app.utils.ErrorHandler
import bayunvnt.base_app.utils.RxUtils

/**
 * Created by bayunvnt on 12/21/16.
 */

class AuthPresenter : BasePresenter()  {

    fun getAuth(username: String, password: String, remember: String, accessToken: String) {
        authView().onShowLoading()
        dataManager!!.getAuth(username, password, accessToken, remember)
                .doOnTerminate { authView().onDismissLoading() }
                .compose<ResponseDataObject<Auth>>(RxUtils.applyApiCall<ResponseDataObject<Auth>>())
                .subscribe({ authResponseDataObject ->
                    if (authResponseDataObject.status == Constant.STATUS_ERROR)
                        authView().onFailed(authResponseDataObject.message!!)
                    else {
                        if (authResponseDataObject.status == Constant.STATUS_SUCCESS) {
                            authView().onSuccess(authResponseDataObject.result!!)
                        } else {
                            authView().onFailed(authResponseDataObject.message!!)
                        }
                    }
                }) { throwable -> ErrorHandler.handlerErrorPresenter(authView(), throwable) }
    }

    internal fun authView(): AuthView {
        return getView()
    }
}
