package bayunvnt.base_app.ui.map

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.PointF
import android.graphics.RectF
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.LinearLayout
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import bayunvnt.base_app.R
import bayunvnt.base_app.base.BaseActivity
import bayunvnt.base_app.model.DataLocation
import bayunvnt.base_app.model.MapProjectPolylines
import bayunvnt.base_app.persistence.Preference
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.geojson.*
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.style.layers.*
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.*
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import kotlinx.android.synthetic.main.activity_mapbox.*
import kotlinx.android.synthetic.main.app_bar_maps.*
import kotlinx.android.synthetic.main.bottomsheet_add_place.*
import kotlinx.android.synthetic.main.bottomsheet_place_info.*


class MapsActivityDrawingMapBox : BaseActivity(), PermissionsListener,
        MapboxMap.OnMapClickListener {

    private var permissionsManager: PermissionsManager? = null
    private var mapboxMap: MapboxMap? = null
    private var firstPointOfPolygon: Point? = null
    var parentlistPolygons: ArrayList<ArrayList<String>> = ArrayList()
    private val listLatLng = ArrayList<LatLng>()
    private var fillLayerPointList: MutableList<Point> = arrayListOf()
    private var lineLayerPointList: MutableList<Point> = arrayListOf()
    private var circleLayerFeatureList: MutableList<Feature> = arrayListOf()

    private var listOfList: MutableList<MutableList<Point>> = arrayListOf()
    private var circleSource: GeoJsonSource? = null
    private var fillSource: GeoJsonSource? = null
    private var lineSource: GeoJsonSource? = null
    private val MARKER_ID = "MARKER_ID"
    private val MARKER_SOURCE_ID = "MARKER_SOURCE_ID"
    private val MARKER_LAYER_ID = "MARKER_LAYER_ID"

    private var ACTIVE_CIRCLE_SOURCE_ID = "circle-source-id"
    private var ACTIVE_FILL_SOURCE_ID = "fill-source-id"
    private var ACTIVE_LINE_SOURCE_ID = "line-source-id"
    private var ACTIVE_CIRCLE_LAYER_ID = "circle-layer-id"
    private var ACTIVE_FILL_LAYER_ID = "fill-layer-polygon-id"
    private var ACTIVE_LINE_LAYER_ID = "line-layer-id"

    private var isAddingKoordinatMaps: Boolean = false

    var bottomSheetDialog: BottomSheetDialog? = null
    var bottomSheetDelete: BottomSheetDialog? = null

    private var polygonLayerList: MutableList<MapProjectPolylines> = arrayListOf()

    var currentId = ""
    lateinit var loadedMapStyle: Style

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token))
        setContentView(R.layout.activity_mapbox)

        currentId = System.currentTimeMillis().toString()

        ACTIVE_CIRCLE_SOURCE_ID = "circle-source-id-$currentId"
        ACTIVE_FILL_SOURCE_ID = "fill-source-id-$currentId"
        ACTIVE_LINE_SOURCE_ID = "line-source-id-$currentId"
        ACTIVE_CIRCLE_LAYER_ID = "circle-layer-id-$currentId"
        ACTIVE_FILL_LAYER_ID = "fill-layer-polygon-id-$currentId"
        ACTIVE_LINE_LAYER_ID = "line-layer-id-$currentId"


        /* set toolbar*/
        if (toolbarMaps != null) {
            setSupportActionBar(toolbarMaps)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            supportActionBar?.setDisplayShowHomeEnabled(false)
        }


        val bsdSort: View = layoutInflater.inflate(R.layout.bottomsheet_add_place, null)
        bottomSheetDialog = BottomSheetDialog(this, R.style.BottomDialogStyle)
        bottomSheetDialog?.setContentView(bsdSort)
        bottomSheetDialog?.setOnShowListener {
            val d = it as BottomSheetDialog
            val bottomSheet = d.findViewById<View>(R.id.llOuter) as LinearLayout
            val coordinatorLayout = bottomSheet.parent as CoordinatorLayout
            val bottomSheetBehavior = BottomSheetBehavior.from(coordinatorLayout.parent as View)
            bottomSheetBehavior.peekHeight = bottomSheet.height
            coordinatorLayout.parent.requestLayout()
        }

        val bsdSort2: View = layoutInflater.inflate(R.layout.bottomsheet_place_info, null)
        bottomSheetDelete = BottomSheetDialog(this)
        bottomSheetDelete?.setContentView(bsdSort2)
        bottomSheetDelete?.setOnShowListener {
            val d = it as BottomSheetDialog
            val bottomSheet = d.findViewById<View>(R.id.llOuter) as LinearLayout
            val coordinatorLayout = bottomSheet.parent as CoordinatorLayout
            val bottomSheetBehavior = BottomSheetBehavior.from(coordinatorLayout.parent as View)
            bottomSheetBehavior.peekHeight = bottomSheet.height
            coordinatorLayout.parent.requestLayout()
        }

        bottomSheetDialog?.btnConfirm?.setOnClickListener {
            if (bottomSheetDialog?.edName?.text.toString().isEmpty()) {
                showToast("Masukkan nama area terlebih dahulu")
            } else if (bottomSheetDialog?.edLuas?.text.toString().isEmpty()) {
                showToast("Masukkan luas area terlebih dahulu")
            } else {

                ACTIVE_CIRCLE_SOURCE_ID = "circle-source-id-$currentId"
                ACTIVE_FILL_SOURCE_ID = "fill-source-id-$currentId"
                ACTIVE_LINE_SOURCE_ID = "line-source-id-$currentId"
                ACTIVE_CIRCLE_LAYER_ID = "circle-layer-id-$currentId"
                ACTIVE_FILL_LAYER_ID = "fill-layer-polygon-id-$currentId"
                ACTIVE_LINE_LAYER_ID = "line-layer-id-$currentId"

                currentId = System.currentTimeMillis().toString()

                val saveKoordinatMaps = DataLocation(currentId, bottomSheetDialog?.edName?.text.toString(), bottomSheetDialog?.edLuas?.text.toString(), listLatLng)
                var locList = Preference.getDataLocation
                if (locList == null) {
                    locList = arrayListOf()
                }
                locList.add(saveKoordinatMaps)
                Preference.saveDataLocation(locList)

                val polylines = MapProjectPolylines()
                polylines.id = currentId
                polylines.CIRCLE_SOURCE_ID = "circle-source-id-${currentId}"
                polylines.FILL_SOURCE_ID = "fill-source-id-${currentId}"
                polylines.LINE_SOURCE_ID = "line-source-id-${currentId}"
                polylines.CIRCLE_LAYER_ID = "circle-layer-id-${currentId}"
                polylines.FILL_LAYER_ID = "fill-layer-id-${currentId}"
                polylines.LINE_LAYER_ID = "line-layer-id-${currentId}"
                polylines.circleSource = initCircleSource(loadedMapStyle, polylines.CIRCLE_SOURCE_ID)
                polylines.fillSource = initFillSource(loadedMapStyle, polylines.FILL_SOURCE_ID)
                polylines.lineSource = initFillSource(loadedMapStyle, polylines.LINE_SOURCE_ID)
                polylines.type = "real"
                val coordinate: MutableList<LatLng> = arrayListOf()
                for (x in listLatLng) {
                    coordinate.add(LatLng(x.latitude, x.longitude))
                }
                polylines.coordinat = coordinate

                listLatLng.clear()

                clearLayerView()

                // Add layers to the map
                initCircleLayer(loadedMapStyle, polylines.CIRCLE_LAYER_ID, polylines.CIRCLE_SOURCE_ID)
                initLineLayer(loadedMapStyle, polylines.LINE_LAYER_ID, polylines.LINE_SOURCE_ID, polylines.CIRCLE_LAYER_ID)
                initFillLayer(loadedMapStyle, polylines.FILL_LAYER_ID, polylines.FILL_SOURCE_ID, polylines.LINE_LAYER_ID, Color.parseColor("#00e9ff"))

                polygonLayerList.add(polylines)
                loadViewLocal(arrayListOf(polylines))
//                setupLocalData(loadedMapStyle)

                map_type_FAB.visibility = View.VISIBLE
                bottomSheetDialog?.edName?.setText("")
                bottomSheetDialog?.edLuas?.setText("")
                bottomSheetDialog?.dismiss()
                showToast("Area berhasil disimpan!")
            }
        }

        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync { mapboxMap ->
            this.mapboxMap = mapboxMap
            initMapInteraction()
            mapboxMap.setStyle(
                    Style.Builder().fromUri(Style.LIGHT)
                            .withImage(MARKER_ID, BitmapFactory.decodeResource(
                                    this.resources, R.drawable.mapbox_marker_icon_default))
                            .withSource(GeoJsonSource(MARKER_SOURCE_ID,
                                    FeatureCollection.fromFeatures(arrayListOf())))
                            .withLayer(SymbolLayer(MARKER_LAYER_ID, MARKER_SOURCE_ID)
                                    .withProperties(
                                            iconImage(MARKER_ID),
                                            iconAllowOverlap(true),
                                            iconIgnorePlacement(true)
                                    )
                            )) { style ->
                loadedMapStyle = style
                mapboxMap.addOnMapClickListener(this)
                // Add sources to the map
                circleSource = initCircleSource(style, ACTIVE_CIRCLE_SOURCE_ID)
                fillSource = initFillSource(style, ACTIVE_FILL_SOURCE_ID)
                lineSource = initLineSource(style, ACTIVE_LINE_SOURCE_ID)

                enableLocationComponent(style)

                // Add layers to the map
                initCircleLayer(style, ACTIVE_CIRCLE_LAYER_ID, ACTIVE_CIRCLE_SOURCE_ID)
                initLineLayer(style, ACTIVE_LINE_LAYER_ID, ACTIVE_LINE_SOURCE_ID, ACTIVE_CIRCLE_LAYER_ID)
                initFillLayer(style, ACTIVE_FILL_LAYER_ID, ACTIVE_FILL_SOURCE_ID, ACTIVE_LINE_LAYER_ID, Color.parseColor("#00e9ff"))

                setupLocalData(style)
            }
        }
    }

    override fun onMapClick(point: LatLng): Boolean {
        val pointf: PointF = mapboxMap!!.projection.toScreenLocation(point!!)
        val rectF = RectF(pointf.x - 10, pointf.y - 10, pointf.x + 10, pointf.y + 10)
        for (i in polygonLayerList) {
            val featureList: List<Feature>? = mapboxMap?.queryRenderedFeatures(rectF, i.FILL_LAYER_ID)
            if (!featureList.isNullOrEmpty() && Preference.getDataLocation != null) {
                for (x in Preference.getDataLocation!!) {
                    if (x.id == i.id) {
                        bottomSheetDelete?.tvName?.text = "Nama Area : ${x.name}"
                        bottomSheetDelete?.tvDesc?.text = "Luas Area : ${x.luas} meter"
                        bottomSheetDelete?.tvOther?.text = ""

                        for ((index, ll) in x.coordinat!!.withIndex()) {
                            bottomSheetDelete?.tvOther?.text = "${bottomSheetDelete?.tvOther?.text}${index + 1}. Latitude : ${ll.latitude} Longitude : ${ll.longitude}\n\n"
                        }

                        bottomSheetDelete?.btnDelete?.setOnClickListener {

                            SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Kondirmasi Hapus data?")
                                    .setContentText("Data area yang dihapus tidak bisa dikembalikan kembali")
                                    .setConfirmText("Hapus")
                                    .setCancelText("Batal")
                                    .setConfirmClickListener { sDialog ->
                                        sDialog.dismissWithAnimation()

                                        val loc = Preference.getDataLocation
                                        loc?.remove(x)
                                        Preference.saveDataLocation(loc!!)

                                        loadedMapStyle.removeLayer(i.LINE_LAYER_ID)
                                        loadedMapStyle.removeLayer(i.CIRCLE_LAYER_ID)
                                        loadedMapStyle.removeLayer(i.FILL_LAYER_ID)
                                        bottomSheetDelete?.dismiss()
                                    }
                                    .setCancelClickListener { sDialog ->
                                        sDialog.dismissWithAnimation()
                                    }
                                    .show()
                        }

                        bottomSheetDelete?.show()
                    }
                }
                return true
            }
        }
        return false
    }


    @SuppressLint("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            val locationComponent = mapboxMap!!.locationComponent
            locationComponent.activateLocationComponent(
                    LocationComponentActivationOptions.builder(this, loadedMapStyle).build())
            locationComponent.isLocationComponentEnabled = true
            locationComponent.cameraMode = CameraMode.TRACKING
            locationComponent.renderMode = RenderMode.COMPASS

            initClick()
        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager?.requestLocationPermissions(this)
        }
    }

    @SuppressLint("NewApi")
    private fun initMapInteraction() {
        mapboxMap?.addOnMapClickListener {
            val pointf = mapboxMap!!.projection.toScreenLocation(it)
            val rectF = RectF(pointf.x - 10, pointf.y - 10, pointf.x + 10, pointf.y + 10)
            val featureList: List<Feature> = mapboxMap!!.queryRenderedFeatures(rectF, ACTIVE_FILL_LAYER_ID)
            if (featureList.isNotEmpty()) {
                for (feature in featureList) {
                    fabAddCoordinate.visibility = View.GONE
                    fabDoneAddCoordinate.visibility = View.GONE
                    val anim = ViewAnimationUtils.createCircularReveal(
                            llActionPickMaps,
                            llActionPickMaps.width - (map_type_FAB.width / 2),
                            map_type_FAB.height / 2,
                            map_type_FAB.width / 2f,
                            llActionPickMaps.width.toFloat())
                    anim.duration = 500
                    anim.interpolator = AccelerateDecelerateInterpolator()
                    anim.addListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationStart(animation: Animator?) {
                            super.onAnimationEnd(animation)
                            llActionPickMaps.visibility = View.VISIBLE
                        }
                    })
                    anim.start()
                }
                true
            }

            //click icon layer
            val selectedFeature: List<Feature> = mapboxMap!!.queryRenderedFeatures(pointf, ACTIVE_CIRCLE_LAYER_ID)
            if (selectedFeature.isNotEmpty()) {

            }
            false
        }

    }

    @SuppressLint("NewApi")
    private fun initClick() {
        btnmyLocation.setOnClickListener {
            val lastKnownLocation = mapboxMap?.locationComponent?.lastKnownLocation

            mapboxMap?.animateCamera(CameraUpdateFactory.newLatLng(
                    LatLng(lastKnownLocation?.latitude!!, lastKnownLocation.longitude)))
        }

        btnZoomIn.setOnClickListener {
            mapboxMap?.animateCamera(CameraUpdateFactory.zoomIn())
        }

        btnZoomOut.setOnClickListener {
            mapboxMap?.animateCamera(CameraUpdateFactory.zoomOut())
        }

        map_type_FAB.setOnClickListener {
            ivcenterAimCoordinat.visibility = View.VISIBLE
            val anim = ViewAnimationUtils.createCircularReveal(
                    llActionPickMaps,
                    llActionPickMaps.width - (map_type_FAB.width / 2),
                    map_type_FAB.height / 2,
                    map_type_FAB.width / 2f,
                    llActionPickMaps.width.toFloat())
            anim.duration = 500
            anim.interpolator = AccelerateDecelerateInterpolator()

            anim.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationStart(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    llActionPickMaps.visibility = View.VISIBLE
                }
            })

            anim.start()
            map_type_FAB.visibility = View.INVISIBLE
        }

        map_type_default.setOnClickListener {
            map_type_default_background.visibility = View.VISIBLE
            map_type_satellite_background.visibility = View.INVISIBLE
            map_type_terrain_background.visibility = View.INVISIBLE
            map_type_default_text.setTextColor(Color.BLUE)
            map_type_satellite_text.setTextColor(Color.parseColor("#808080"))
            map_type_terrain_text.setTextColor(Color.parseColor("#808080"))
//            mapboxMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL

            mapboxMap?.setStyle(
                    Style.Builder().fromUri(Style.LIGHT)
                            .withImage(MARKER_ID, BitmapFactory.decodeResource(
                                    this.resources, R.drawable.mapbox_marker_icon_default))
                            .withSource(GeoJsonSource(MARKER_SOURCE_ID,
                                    FeatureCollection.fromFeatures(arrayListOf())))
                            .withLayer(SymbolLayer(MARKER_LAYER_ID, MARKER_SOURCE_ID)
                                    .withProperties(
                                            iconImage(MARKER_ID),
                                            iconAllowOverlap(true),
                                            iconIgnorePlacement(true)
                                    )
                            )) { style ->

                loadedMapStyle = style
                mapboxMap?.addOnMapClickListener(this)
                // Add sources to the map
                circleSource = initCircleSource(style, ACTIVE_CIRCLE_SOURCE_ID)
                fillSource = initFillSource(style, ACTIVE_FILL_SOURCE_ID)
                lineSource = initLineSource(style, ACTIVE_LINE_SOURCE_ID)

                // Add layers to the maps
                initCircleLayer(style, ACTIVE_CIRCLE_LAYER_ID, ACTIVE_CIRCLE_SOURCE_ID)
                initLineLayer(style, ACTIVE_LINE_LAYER_ID, ACTIVE_LINE_SOURCE_ID, ACTIVE_CIRCLE_LAYER_ID)
                initFillLayer(style, ACTIVE_FILL_LAYER_ID, ACTIVE_FILL_SOURCE_ID, ACTIVE_LINE_LAYER_ID, Color.parseColor("#00e9ff"))

                if (circleSource != null) {
                    circleSource!!.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList))
                }
                if (lineSource != null) {
                    lineSource!!.setGeoJson(FeatureCollection.fromFeatures(arrayOf(Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList)))))
                }

                val finalFeatureList: MutableList<Feature> = java.util.ArrayList()
                finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)))
                val newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList)
                if (fillSource != null) {
                    fillSource!!.setGeoJson(newFeatureCollection)
                }

                setupLocalData(style)
            }
        }

        // Handle selection of the Satellite map type
        map_type_satellite.setOnClickListener {
            map_type_default_background.visibility = View.INVISIBLE
            map_type_satellite_background.visibility = View.VISIBLE
            map_type_terrain_background.visibility = View.INVISIBLE
            map_type_default_text.setTextColor(Color.parseColor("#808080"))
            map_type_satellite_text.setTextColor(Color.BLUE)
            map_type_terrain_text.setTextColor(Color.parseColor("#808080"))
//            mMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID
            mapboxMap?.setStyle(
                    Style.Builder().fromUri(Style.SATELLITE_STREETS)
                            .withImage(MARKER_ID, BitmapFactory.decodeResource(
                                    this.resources, R.drawable.mapbox_marker_icon_default))
                            .withSource(GeoJsonSource(MARKER_SOURCE_ID,
                                    FeatureCollection.fromFeatures(arrayListOf())))
                            .withLayer(SymbolLayer(MARKER_LAYER_ID, MARKER_SOURCE_ID)
                                    .withProperties(
                                            iconImage(MARKER_ID),
                                            iconAllowOverlap(true),
                                            iconIgnorePlacement(true)
                                    )
                            )) { style ->
                loadedMapStyle = style
                mapboxMap?.addOnMapClickListener(this)
                // Add sources to the map
                circleSource = initCircleSource(style, ACTIVE_CIRCLE_SOURCE_ID)
                fillSource = initFillSource(style, ACTIVE_FILL_SOURCE_ID)
                lineSource = initLineSource(style, ACTIVE_LINE_SOURCE_ID)

//                enableLocationComponent(style)

                // Add layers to the map
                initCircleLayer(style, ACTIVE_CIRCLE_LAYER_ID, ACTIVE_CIRCLE_SOURCE_ID)
                initLineLayer(style, ACTIVE_LINE_LAYER_ID, ACTIVE_LINE_SOURCE_ID, ACTIVE_CIRCLE_LAYER_ID)
                initFillLayer(style, ACTIVE_FILL_LAYER_ID, ACTIVE_FILL_SOURCE_ID, ACTIVE_LINE_LAYER_ID, Color.parseColor("#00e9ff"))

                if (circleSource != null) {
                    circleSource!!.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList))
                }
                if (lineSource != null) {
                    lineSource!!.setGeoJson(FeatureCollection.fromFeatures(arrayOf(Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList)))))
                }

                val finalFeatureList: MutableList<Feature> = java.util.ArrayList()
                finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)))
                val newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList)
                if (fillSource != null) {
                    fillSource!!.setGeoJson(newFeatureCollection)
                }

                setupLocalData(style)
            }
        }

        // Handle selection of the terrain map type
        map_type_terrain.setOnClickListener {
            map_type_default_background.visibility = View.INVISIBLE
            map_type_satellite_background.visibility = View.INVISIBLE
            map_type_terrain_background.visibility = View.VISIBLE
            map_type_default_text.setTextColor(Color.parseColor("#808080"))
            map_type_satellite_text.setTextColor(Color.parseColor("#808080"))
            map_type_terrain_text.setTextColor(Color.BLUE)
            mapboxMap?.setStyle(
                    Style.Builder().fromUri(Style.DARK)
                            .withImage(MARKER_ID, BitmapFactory.decodeResource(
                                    this.resources, R.drawable.mapbox_marker_icon_default))
                            .withSource(GeoJsonSource(MARKER_SOURCE_ID,
                                    FeatureCollection.fromFeatures(arrayListOf())))
                            .withLayer(SymbolLayer(MARKER_LAYER_ID, MARKER_SOURCE_ID)
                                    .withProperties(
                                            iconImage(MARKER_ID),
                                            iconAllowOverlap(true),
                                            iconIgnorePlacement(true)
                                    )
                            )) { style ->
                loadedMapStyle = style
                mapboxMap?.addOnMapClickListener(this)
                // Add sources to the map
                circleSource = initCircleSource(style, ACTIVE_CIRCLE_SOURCE_ID)
                fillSource = initFillSource(style, ACTIVE_FILL_SOURCE_ID)
                lineSource = initLineSource(style, ACTIVE_LINE_SOURCE_ID)

//                enableLocationComponent(style)

                // Add layers to the map
                initCircleLayer(style, ACTIVE_CIRCLE_LAYER_ID, ACTIVE_CIRCLE_SOURCE_ID)
                initLineLayer(style, ACTIVE_LINE_LAYER_ID, ACTIVE_LINE_SOURCE_ID, ACTIVE_CIRCLE_LAYER_ID)
                initFillLayer(style, ACTIVE_FILL_LAYER_ID, ACTIVE_FILL_SOURCE_ID, ACTIVE_LINE_LAYER_ID, Color.parseColor("#00e9ff"))

                if (circleSource != null) {
                    circleSource!!.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList))
                }
                if (lineSource != null) {
                    lineSource!!.setGeoJson(FeatureCollection.fromFeatures(arrayOf(Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList)))))
                }

                val finalFeatureList: MutableList<Feature> = java.util.ArrayList()
                finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)))
                val newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList)
                if (fillSource != null) {
                    fillSource!!.setGeoJson(newFeatureCollection)
                }
                setupLocalData(style)
            }
        }

        mapboxMap?.addOnCameraIdleListener {

            val midlatLang: LatLng = mapboxMap!!.cameraPosition.target

            fabAddCoordinate.setOnClickListener {
                // Use the map click location to create a Point object
                val mapTargetPoint = Point.fromLngLat(mapboxMap!!.cameraPosition.target.longitude,
                        mapboxMap!!.cameraPosition.target.latitude)


                // Make note of the first map click location so that it can be used to create a closed polygon later on
                if (circleLayerFeatureList.isEmpty()) {
                    firstPointOfPolygon = mapTargetPoint
                }
                // Add the click point to the circle layer and update the display of the circle layer data

                // Add the click point to the circle layer and update the display of the circle layer data
                circleLayerFeatureList.add(Feature.fromGeometry(mapTargetPoint))
                if (circleSource != null) {
                    circleSource!!.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList))
                }

                // Add the click point to the line layer and update the display of the line layer data
                when {
                    circleLayerFeatureList.size < 3 -> {
                        lineLayerPointList.add(mapTargetPoint)
                    }
                    circleLayerFeatureList.size == 3 -> {
                        lineLayerPointList.add(mapTargetPoint)
                        lineLayerPointList.add(firstPointOfPolygon!!)
                    }
                    else -> {
                        lineLayerPointList.removeAt(circleLayerFeatureList.size - 1)
                        lineLayerPointList.add(mapTargetPoint)
                        lineLayerPointList.add(firstPointOfPolygon!!)
                    }
                }
                if (lineSource != null) {
                    lineSource!!.setGeoJson(FeatureCollection.fromFeatures(arrayOf(Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList)))))
                }

                // Add the click point to the fill layer and update the display of the fill layer data
                when {
                    circleLayerFeatureList.size < 3 -> {
                        fillLayerPointList.add(mapTargetPoint)
                    }
                    circleLayerFeatureList.size == 3 -> {
                        fillLayerPointList.add(mapTargetPoint)
                        fillLayerPointList.add(firstPointOfPolygon!!)
                    }
                    else -> {
                        fillLayerPointList.removeAt(fillLayerPointList.size - 1)
                        fillLayerPointList.add(mapTargetPoint)
                        fillLayerPointList.add(firstPointOfPolygon!!)
                    }
                }

                listOfList = arrayListOf()
                listOfList.add(fillLayerPointList)
                val finalFeatureList: MutableList<Feature> = java.util.ArrayList()
                finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)))
                val newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList)
                if (fillSource != null) {
                    fillSource!!.setGeoJson(newFeatureCollection)
                }

                val LatLong: String = mapTargetPoint.latitude().toString() + "," + mapTargetPoint.longitude().toString()

                listLatLng.add(midlatLang)
            }
        }

        fabClearCoordinate.setOnClickListener {
            clearEntireMap()
        }

        btnCancel.setOnClickListener {

            SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Kondirmasi Hapus data?")
                    .setContentText("Data area yang dihapus tidak bisa dikembalikan kembali")
                    .setConfirmText("Hapus")
                    .setCancelText("Batal")
                    .setConfirmClickListener { sDialog ->
                        sDialog.dismissWithAnimation()

                        clearLayerView()

                        fabAddCoordinate.visibility = View.VISIBLE
                        fabDoneAddCoordinate.visibility = View.VISIBLE
                        val anim = ViewAnimationUtils.createCircularReveal(
                                llActionPickMaps,
                                llActionPickMaps.width - (map_type_FAB.width / 2),
                                map_type_FAB.height / 2,
                                llActionPickMaps.width.toFloat(),
                                map_type_FAB.width / 2f)
                        anim.duration = 200
                        anim.interpolator = AccelerateDecelerateInterpolator()
                        anim.addListener(object : AnimatorListenerAdapter() {
                            override fun onAnimationEnd(animation: Animator?) {
                                super.onAnimationEnd(animation)
                                llActionPickMaps.visibility = View.INVISIBLE
                            }
                        })
                        anim.start()
                        isAddingKoordinatMaps = true
                        map_type_FAB.visibility = View.VISIBLE

                    }
                    .setCancelClickListener { sDialog ->
                        sDialog.dismissWithAnimation()
                    }
                    .show()
        }

        fabDoneAddCoordinate.setOnClickListener {
            if (listLatLng.size > 2) {
                ivcenterAimCoordinat.visibility = View.INVISIBLE
                val anim = ViewAnimationUtils.createCircularReveal(
                        llActionPickMaps,
                        llActionPickMaps.width - (map_type_FAB.width / 2),
                        map_type_FAB.height / 2,
                        llActionPickMaps.width.toFloat(),
                        map_type_FAB.width / 2f)
                anim.duration = 200
                anim.interpolator = AccelerateDecelerateInterpolator()

                anim.addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator?) {
                        super.onAnimationEnd(animation)
                        llActionPickMaps.visibility = View.INVISIBLE
                    }
                })
                anim.start()

                Handler().postDelayed({
                    SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Simpan data?")
                            .setContentText("Pastikan kembali area sebelum menyimpan!")
                            .setConfirmText("Simpan")
                            .setCancelText("Batal")
                            .setConfirmClickListener { sDialog ->
                                sDialog.dismissWithAnimation()

                                bottomSheetDialog?.edName?.requestFocus()
                                bottomSheetDialog?.show()
                            }
                            .setCancelClickListener { sDialog ->
                                sDialog.dismissWithAnimation()
                                ivcenterAimCoordinat.visibility = View.VISIBLE
                                // Start animator to reveal the selection view, starting from the FAB itself
                                val anim = ViewAnimationUtils.createCircularReveal(
                                        llActionPickMaps,
                                        llActionPickMaps.width - (map_type_FAB.width / 2),
                                        map_type_FAB.height / 2,
                                        map_type_FAB.width / 2f,
                                        llActionPickMaps.width.toFloat())
                                anim.duration = 500
                                anim.interpolator = AccelerateDecelerateInterpolator()

                                anim.addListener(object : AnimatorListenerAdapter() {
                                    override fun onAnimationStart(animation: Animator?) {
                                        super.onAnimationEnd(animation)
                                        llActionPickMaps.visibility = View.VISIBLE
                                    }
                                })

                                anim.start()
                            }
                            .show()
                }, 400)


                //lisMauDikirim = ["1.121,32.3;23.332,432.32;43.32,32.43","1.121,32.3;23.332,432.32;43.32,32.43"];
            } else {
                Toast.makeText(this, "Tentukan titik minimal 3 titik koordinat", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun setupLocalData(style: Style) {
        val results = Preference.getDataLocation
        polygonLayerList = arrayListOf()

        if (!results.isNullOrEmpty()) {
            for (i in results) {
                if(loadedMapStyle.getSource("circle-source-id-${i.id}") == null &&
                        loadedMapStyle.getSource("fill-source-id-${i.id}") == null &&
                        loadedMapStyle.getSource("line-source-id-${i.id}") == null ){
                    val polylines = MapProjectPolylines()
                    polylines.id = i.id
                    polylines.CIRCLE_SOURCE_ID = "circle-source-id-${i.id}"
                    polylines.FILL_SOURCE_ID = "fill-source-id-${i.id}"
                    polylines.LINE_SOURCE_ID = "line-source-id-${i.id}"
                    polylines.CIRCLE_LAYER_ID = "circle-layer-id-${i.id}"
                    polylines.FILL_LAYER_ID = "fill-layer-id-${i.id}"
                    polylines.LINE_LAYER_ID = "line-layer-id-${i.id}"
                    polylines.circleSource = initCircleSource(style, polylines.CIRCLE_SOURCE_ID)
                    polylines.fillSource = initFillSource(style, polylines.FILL_SOURCE_ID)
                    polylines.lineSource = initFillSource(style, polylines.LINE_SOURCE_ID)
                    polylines.type = "real"
                    val coordinate: MutableList<LatLng> = arrayListOf()
                    for (x in i.coordinat!!) {
                        coordinate.add(LatLng(x.latitude, x.longitude))
                    }
                    polylines.coordinat = coordinate

                    // Add layers to the map
                    initCircleLayer(style, polylines.CIRCLE_LAYER_ID, polylines.CIRCLE_SOURCE_ID)
                    initLineLayer(style, polylines.LINE_LAYER_ID, polylines.LINE_SOURCE_ID, polylines.CIRCLE_LAYER_ID)
                    initFillLayer(style, polylines.FILL_LAYER_ID, polylines.FILL_SOURCE_ID, polylines.LINE_LAYER_ID, Color.parseColor("#00e9ff"))

                    polygonLayerList.add(polylines)

                    Log.d("popopox :", "x")
                }
            }

            loadViewLocal(polygonLayerList)
            Log.d("results", results.toString())
        } else {
            Log.e("data", "kosong")
        }
    }

    private fun loadViewLocal(pol: MutableList<MapProjectPolylines>) {

        if (!pol.isNullOrEmpty()) {
            for (i in pol) {
                val coodinateToArrayList: ArrayList<LatLng> = ArrayList()
                coodinateToArrayList.addAll(i.coordinat!!)

                i.firstPointOfPolygon = Point.fromLngLat(coodinateToArrayList[0].longitude, coodinateToArrayList[0].latitude)

                for ((index, data) in coodinateToArrayList.withIndex()) {
                    val mapTargetPoint = Point.fromLngLat(data.longitude, data.latitude)

                    //set data titik or dot
                    i.circleLayerFeatureList.add(Feature.fromGeometry(mapTargetPoint))

                    //set data line
                    when {
                        index < 2 -> {
                            i.lineLayerPointList.add(mapTargetPoint)
                        }
                        index == 2 -> {
                            i.lineLayerPointList.add(mapTargetPoint)
                            i.lineLayerPointList.add(i.firstPointOfPolygon!!)
                        }
                        else -> {
                            i.lineLayerPointList.removeAt(index)
                            i.lineLayerPointList.add(mapTargetPoint)
                            i.lineLayerPointList.add(i.firstPointOfPolygon!!)
                        }
                    }

                    //set data filler
                    when {
                        index < 2 -> {
                            i.fillLayerPointList.add(mapTargetPoint)
                        }
                        index == 2 -> {
                            i.fillLayerPointList.add(mapTargetPoint)
                            i.fillLayerPointList.add(i.firstPointOfPolygon!!)
                        }
                        else -> {
                            i.fillLayerPointList.removeAt(index)
                            i.fillLayerPointList.add(mapTargetPoint)
                            i.fillLayerPointList.add(i.firstPointOfPolygon!!)
                        }
                    }

                    i.listOfList = arrayListOf()
                    i.listOfList.add(i.fillLayerPointList)

                    i.circleSource?.setGeoJson(FeatureCollection.fromFeatures(i.circleLayerFeatureList))
                    i.lineSource!!.setGeoJson(FeatureCollection.fromFeatures(arrayOf(Feature.fromGeometry(LineString.fromLngLats(i.lineLayerPointList)))))

                    val finalFeatureList: MutableList<Feature> = java.util.ArrayList()
                    finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(i.listOfList)))
                    val newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList)
                    i.fillSource?.setGeoJson(newFeatureCollection)


                    Log.d("popopoy :", "${i.CIRCLE_LAYER_ID}")
                }
            }
        }
    }

    /**
     * Remove the drawn area from the map by resetting the FeatureCollections used by the layers' sources
     */
    @SuppressLint("NewApi")
    private fun clearEntireMap() {
        SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Kondirmasi Hapus data?")
                .setContentText("Data area yang dihapus tidak bisa dikembalikan kembali")
                .setConfirmText("Hapus")
                .setCancelText("Batal")
                .setConfirmClickListener { sDialog ->
                    sDialog.dismissWithAnimation()

                    clearLayerView()

                }
                .setCancelClickListener { sDialog ->
                    sDialog.dismissWithAnimation()
                }
                .show()

    }

    private fun clearLayerView() {
        listLatLng.clear()
        fillLayerPointList = java.util.ArrayList()
        circleLayerFeatureList = java.util.ArrayList()
        lineLayerPointList = java.util.ArrayList()
        circleSource?.setGeoJson(FeatureCollection.fromFeatures(arrayOf()))
        lineSource?.setGeoJson(FeatureCollection.fromFeatures(arrayOf()))
        fillSource?.setGeoJson(FeatureCollection.fromFeatures(arrayOf()))
    }

    /**
     * Set up the CircleLayer source for showing map click points
     */
    private fun initCircleSource(loadedMapStyle: Style, CIRCLE_SOURCE_ID: String): GeoJsonSource? {
        val circleFeatureCollection = FeatureCollection.fromFeatures(arrayOf())
        val circleGeoJsonSource = GeoJsonSource(CIRCLE_SOURCE_ID, circleFeatureCollection)
        loadedMapStyle.addSource(circleGeoJsonSource)
        return circleGeoJsonSource
    }

    /**
     * Set up the CircleLayer for showing polygon click points
     */
    private fun initCircleLayer(loadedMapStyle: Style, CIRCLE_LAYER_ID: String, CIRCLE_SOURCE_ID: String) {
        val circleLayer = SymbolLayer(CIRCLE_LAYER_ID,
                CIRCLE_SOURCE_ID)
        circleLayer.withProperties(
                iconImage(MARKER_ID),
                iconAllowOverlap(true),
                iconIgnorePlacement(true)
        )
        loadedMapStyle.addLayer(circleLayer)
    }

    /**
     * Set up the FillLayer source for showing map click points
     */
    private fun initFillSource(loadedMapStyle: Style, FILL_SOURCE_ID: String): GeoJsonSource? {
        val fillFeatureCollection = FeatureCollection.fromFeatures(arrayOf())
        val fillGeoJsonSource = GeoJsonSource(FILL_SOURCE_ID, fillFeatureCollection)
        loadedMapStyle.addSource(fillGeoJsonSource)
        return fillGeoJsonSource
    }

    /**
     * Set up the FillLayer for showing the set boundaries' polygons
     */
    private fun initFillLayer(loadedMapStyle: Style, FILL_LAYER_ID: String, FILL_SOURCE_ID: String, LINE_LAYER_ID: String, colorSource: Int) {
        val fillLayer = FillLayer(FILL_LAYER_ID,
                FILL_SOURCE_ID)
        fillLayer.setProperties(
                fillOpacity(.6f),
                fillColor(colorSource)
        )
        loadedMapStyle.addLayerBelow(fillLayer, LINE_LAYER_ID)
    }

    /**
     * Set up the LineLayer source for showing map click points
     */
    private fun initLineSource(loadedMapStyle: Style, LINE_SOURCE_ID: String): GeoJsonSource? {
        val lineFeatureCollection = FeatureCollection.fromFeatures(arrayOf())
        val lineGeoJsonSource = GeoJsonSource(LINE_SOURCE_ID, lineFeatureCollection)
        loadedMapStyle.addSource(lineGeoJsonSource)
        return lineGeoJsonSource
    }

    /**
     * Set up the LineLayer for showing the set boundaries' polygons
     */
    private fun initLineLayer(loadedMapStyle: Style, LINE_LAYER_ID: String, LINE_SOURCE_ID: String, CIRCLE_LAYER_ID: String) {
        val lineLayer = LineLayer(LINE_LAYER_ID,
                LINE_SOURCE_ID)
        lineLayer.setProperties(
                PropertyFactory.lineColor(Color.WHITE),
                PropertyFactory.lineWidth(1f)
        )
        loadedMapStyle.addLayerBelow(lineLayer, CIRCLE_LAYER_ID)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permissionsManager?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onExplanationNeeded(permissionsToExplain: List<String?>?) {
        Toast.makeText(this, "Harap aktifkan lokasi",
                Toast.LENGTH_LONG).show()
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            mapboxMap!!.getStyle { style -> enableLocationComponent(style) }
        } else {
            Toast.makeText(this, "Permission tidak diberi akses", Toast.LENGTH_LONG).show()
        }
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item?.itemId) {
            R.id.action_main_menu -> {
                /*startActivity(Intent(this, NotificationActivity::class.java))
                countNotif = 0*/
                openCloseDrawer()
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("RtlHardcoded")
    private fun openCloseDrawer() {
        if (map_type_selection.visibility == View.VISIBLE) {
            map_type_selection.visibility = View.GONE
        } else {
            map_type_selection.visibility = View.VISIBLE
        }
    }

    @SuppressLint("RtlHardcoded")
    private fun closeNavigationDrawer() {
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) drawerLayout.closeDrawer(Gravity.RIGHT)
    }

    companion object {
        internal const val TAG = "MapsActivityDrawing"
        private const val DEFAULT_ZOOM: Float = 18f
        fun getStartIntent(context: Context): Intent {
            return Intent(context, MapsActivityDrawingMapBox::class.java)
        }
    }
}
