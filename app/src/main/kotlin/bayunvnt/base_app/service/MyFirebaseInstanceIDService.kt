//package bayunvnt.base_app.service
//
///**
// * Created by Anonymous on 8/10/2017.
// */
//
//import android.content.Intent
//import android.support.v4.content.LocalBroadcastManager
//import android.util.Log
//import com.google.firebase.iid.FirebaseInstanceId
//import com.google.firebase.iid.FirebaseInstanceIdService
//import bayunvnt.base_app.constant.Constant
//
//class MyFirebaseInstanceIDService : FirebaseInstanceIdService() {
//
//    override fun onTokenRefresh() {
//        super.onTokenRefresh()
//        var refreshedToken = FirebaseInstanceId.getInstance().token
//
//        // Saving reg id to shared preferences
//        storeRegIdInPref(refreshedToken)
//
//        Log.d("firebase", refreshedToken)
//        // sending reg id to your server
//        sendRegistrationToServer(refreshedToken)
//
//        // Notify UI that registration has completed, so the progress indicator can be hidden.
//        var registrationComplete = Intent(Constant.REGISTRATION_COMPLETE)
//        registrationComplete.putExtra("token", refreshedToken)
//        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete)
//    }
//
//    private fun sendRegistrationToServer(token: String?) {
//        // sending gcm token to server
//        Log.e(TAG, "sendRegistrationToServer: " + token!!)
//    }
//
//    private fun storeRegIdInPref(token: String?) {
//        var pref = applicationContext.getSharedPreferences(Constant.SHARED_PREF, 0)
//        var editor = pref.edit()
//        editor.putString("regId", token)
//        editor.commit()
//    }
//
//    companion object {
//        private val TAG = MyFirebaseInstanceIDService::class.java.simpleName
//    }
//}
